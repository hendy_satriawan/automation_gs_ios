*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot


*** Variables ***
${cari_Penyakit}    Bronkitis


*** Keywords ***
Side Menu Penyakit
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  # buka drawer menu
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Penyakit"]     ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Penyakit"]
  # masuk ke halaman Penyakit
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Penyakit"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari Penyakit"]     ${timeout}
  # buka penyakit abses gigi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Abses Gigi"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Abses Gigi"]
  # masuk ke penyakit abses gigi & tab deskripsi
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Penyakit"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]      ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Abses Gigi"]
  # masuk ke tab pencegahan
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Pencegahan, tab, 2 of 6"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Pencegahan, tab, 2 of 6"]
  # masuk ke tab gejala
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Gejala, tab, 3 of 6"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Gejala, tab, 3 of 6"]
  # masuk ke tab penyebab
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Penyebab, tab, 4 of 6"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Penyebab, tab, 4 of 6"]
  # masuk ke tab diagnosa
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Diagnosis, tab, 5 of 6"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Diagnosis, tab, 5 of 6"]
  # share penyakit
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Kembali Ke Halaman Penyakit Dari Detail Penyakit
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # cek halaman Penyakit
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Penyakit"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari Penyakit"]     ${timeout}

Cari Penyakit
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari Penyakit"]   ${timeout}
  Click Element    //XCUIElementTypeSearchField[@name="Cari Penyakit"]
  Input Text    //XCUIElementTypeSearchField[@name="Cari Penyakit"]    ${cari_Penyakit}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # cek hasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${cari_Penyakit}"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="${cari_Penyakit}"]
