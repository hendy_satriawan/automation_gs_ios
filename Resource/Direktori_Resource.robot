*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Kembali ke Direktori dokter dari detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}

Kembali ke Direktori Rumah Sakit dari Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rumah Sakit"]    ${timeout}

Kembali ke Direktori Klinik dari Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Klinik"]    ${timeout}

Kembali ke Direktori Gym & Health Club dari Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Gym & Health Club"]    ${timeout}

Kembali ke Direktori Spa & Massage dari Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Spa & Massage"]    ${timeout}

Kembali ke Direktori Healthy Food & Beverage dari Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Healthy Food & Beverage"]    ${timeout}

Kembali ke Direktori Beauty dari Detil
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Beauty"]    ${timeout}

Kembali ke Direktori Lab dari Detil
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Lab"]   ${timeout}

Kembali ke Direktori Praktisi dari Detil
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Praktisi"]   ${timeout}


Direktori Dokter & Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Direktori"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Direktori"]
  #masuk ke direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  # buka detail dokter
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Rating"]
  #masuk ke halaman dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rekomendasi"]   ${timeout}
  #tab profile
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Profil')]   ${timeout}
  # tab jadwal
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jadwal"]    ${timeout}
  # tab Komentar
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Komentar')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Komentar')]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # muncul opsi share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  # kembali ke detail
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}

Direktori Dokter Filter & Hapus Filter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon filter"]
  #masuk ke halaman filter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close blue icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Reset"]   ${timeout}
  # pilih spesialis
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Spesialis"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Spesialis"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Gigi"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Gigi"]
  # klik selesai
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # lihat hasil filter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Gigi"]     ${timeout}
  # hapus filter --------------
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon filter"]
  #masuk ke halaman filter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Reset"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Reset"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # lihat halaman direktori setelah reset filter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon sort list"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon filter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Page Should Not Contain Element    //XCUIElementTypeStaticText[@name="Gigi"]    ${timeout}


Direktori Rumah Sakit & Detail
  #masuk ke direktori rumah sakit
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Dokter"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Dokter"]
  # buka pilihan direktori
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Direktori"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Rumah Sakit"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Rumah Sakit"]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="checklist_circle_icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # masuk ke halaman direktori rumah sakit
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rumah Sakit"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="dir_badge_icon"]    ${timeout}
  # buka detail rumah sakit
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Rating"]
  #masuk ke halaman rumah sakit
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rumah Sakit"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rekomendasi"]   ${timeout}
  # tab profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Rumah Sakit"]   ${timeout}
  # tab fasilitas
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Fasilitas')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Fasilitas')]
  # tab layanan
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Layanan')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Layanan')]
  # tab Komentar
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Komentar')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Komentar')]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # tab dokter
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Dokter')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Dokter')]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="dir_rs_icon"]    ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # muncul opsi share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]


Direktori Klinik & Detail
  #masuk ke direktori klinik
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rumah Sakit"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Rumah Sakit"]
  # buka pilihan direktori
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Direktori"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Klinik"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Klinik"]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="checklist_circle_icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # masuk ke halaman direktori Klinik
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Klinik"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Klinik"]    ${timeout}
  # buka detail klinik
  Click Element    //XCUIElementTypeStaticText[@name="Klinik"]
  #masuk ke halaman klinik
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Klinik"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rekomendasi"]   ${timeout}
  # tab profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil"]   ${timeout}
  # tab dokter
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Dokter')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Dokter')]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="icon_search"]   ${timeout}
  # tab jadwal
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jadwal"]   ${timeout}
  # tab Komentar
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Komentar')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Komentar')]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Direktori Gym Health Club & Detail
  #masuk ke direktori Gym & Health Club
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Klinik"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Klinik"]
  # buka pilihan direktori
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Direktori"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Gym & Health Club"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Gym & Health Club"]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="checklist_circle_icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # masuk ke halaman direktori Gym & Health Club
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Gym & Health Club"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  # buka detail Gym & Health Club
  ${gymdewasa}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Gym Dewasa"]
  Log    ${gymdewasa}
  ${yoga}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Yoga"]
  Log    ${yoga}
  Run Keyword If    ${gymdewasa}    Click Element    //XCUIElementTypeStaticText[@name="Gym Dewasa"]
  Run Keyword Unless    ${gymdewasa}    Click Element    //XCUIElementTypeStaticText[@name="Yoga"]
  #masuk ke halaman Gym & Health Club
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Gym & Health Club"]   ${timeout}
  # tab profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil"]   ${timeout}
  # tab Jadwal
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jadwal"]   ${timeout}
  # tab komentar
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Komentar')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Komentar')]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Direktori Spa Massage & Detail
  #masuk ke direktori Spa & Massage
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Gym & Health Club"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Gym & Health Club"]
  # buka pilihan direktori
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Direktori"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="checklist_circle_icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # masuk ke halaman direktori Spa & Massage
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Spa & Massage"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  # buka detail Spa & Massage
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]
  #masuk ke halaman Gym & Health Club
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Spa & Massage"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]   ${timeout}
  # tab profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil"]   ${timeout}
  # tab jadwal
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]
  # tab komentar
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Komentar')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Komentar')]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]


Direktori Healthy Food Beverage & Detail
  #masuk ke direktori Healthy Food & Beverage
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Spa & Massage"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Spa & Massage"]
  # buka pilihan direktori
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Direktori"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Healthy Food & Beverage"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Healthy Food & Beverage"]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="checklist_circle_icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # masuk ke halaman direktori Healthy Food & Beverage
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Healthy Food & Beverage"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  # buka detail Healthy Food & Beverage
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Healthy Beverage"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Healthy Beverage"]
  #masuk ke halaman Healthy Food & Beverage
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Healthy Food & Beverage"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rekomendasi"]   ${timeout}
  # tab profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil"]   ${timeout}
  # tab jadwal
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jadwal"]   ${timeout}
  # tab komentar
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Komentar')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Komentar')]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Direktori Beauty & Detail
  #masuk ke direktori Beauty
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Healthy Food & Beverage"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Healthy Food & Beverage"]
  # buka pilihan direktori
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Direktori"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Beauty"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Beauty"]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="checklist_circle_icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # masuk ke halaman direktori Beauty
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Beauty"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Rating"]
  #masuk ke halaman Beauty
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Beauty"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rekomendasi"]   ${timeout}
  # tab profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil"]   ${timeout}
  # tab jadwal
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jadwal"]   ${timeout}
  # tab komentar
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Komentar')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Komentar')]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Direktori Lab & Detail
  #masuk ke direktori Lab
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Beauty"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Beauty"]
  # buka pilihan direktori
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Direktori"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Lab"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Lab"]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="checklist_circle_icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # masuk ke halaman direktori Lab
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Laboratorium Klinik"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Lab"]    ${timeout}
  # buka detail Lab
  Click Element    //XCUIElementTypeStaticText[@name="Laboratorium Klinik"]
  #masuk ke halaman Lab
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Lab"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rekomendasi"]   ${timeout}
  # tab profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil"]   ${timeout}
  # tab jadwal
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jadwal"]   ${timeout}
  # tab komentar
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Komentar')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Komentar')]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Direktori Praktisi & Detail
  #masuk ke direktori Praktisi
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Lab"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Lab"]
  # buka pilihan direktori
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Direktori"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Praktisi"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Praktisi"]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="checklist_circle_icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Selesai"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Selesai"]
  # masuk ke halaman direktori Praktisi
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Praktisi"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Rating"]
  #masuk ke halaman Praktisi
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Praktisi"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rekomendasi"]   ${timeout}
  # tab profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil"]   ${timeout}
  # tab jadwal
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Jadwal')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jadwal"]   ${timeout}
  # tab komentar
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Komentar')]    ${timeout}
  Click Element    //XCUIElementTypeCell[contains(@name,'Komentar')]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
