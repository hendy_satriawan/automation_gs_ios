*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    String

Resource    ../Resource/Permission_Resource.robot

*** Variables ***
# ${REMOTE_URL}     http://localhost:4723/wd/hub
# ${PLATFORM_NAME}    Android
# ${PLATFORM_VERSION_EMULATOR}    6.0
# ${PLATFORM_VERSION_REAL}       7.1.1
# ${DEVICE_NAME_EMULATOR}    192.168.56.101:5555
# ${DEVICE_NAME_REAL}      192.168.90.222:5555  #49ffe4aa
# ${APP}            com.guesehat.android
# ${APP_PACKAGE}    com.guesehat.android
# ${APP_ACTIVITY}    com.guesehat.android.feature.splashscreen.SplashScreenActivity
# ${APP_ACTIVITY_SPLASH}  com.guesehat.android.feature.splashscreen.SplashScreenActivity
# ${APP2}            com.android.chrome
# ${APP_PACKAGE2}    com.android.chrome
# ${APP_ACTIVITY2}    com.google.android.apps.chrome.Main
# ${APP_ACTIVITY_SPLASH2}  com.google.android.apps.chrome.Main
# ${timeout}    50s
${email_login}    hendy.satriawan@gmail.com
${pass_login}     1234567
${first_name}   Hendy
${last_name}    GUE
${email_regis}    hendy.satriawan@gmail.com
${pass_regis}   guegue
${email_fb}   fetzune@gmail.com
${pass_fb}    Awan@123
${name_login_fb}    AAM JAMILAH
${email_login_google}    hendy.satriawan@gmail.com
${pass_login_google}    manusiamanusia
${nama_akun_google}   Hendy Satriawan
${nama_akun_login_gs}   Hendy Satriawan
${nama_akun_fb}     fetuna hananti
${timeout}    30s
${yangdicari}     Makanan Yang Sehat dan Murah
# ${automationName1}    UiAutomator
# ${automationName2}    UiAutomator2
${REMOTE_URL}       http://0.0.0.0:4723/wd/hub
${app}    /Users/hendy.satriawan_gue/Documents/GS_ios_app/GueSehat.app
${bundleId}   com.guesehat.ios
${version}    12.1
${deviceName}    iphone 7 Plus
${udid}    C4D26399-52D4-4118-B641-0107D993289D
${platformName}    iOS
${automationName}    XCUITest

*** Keywords ***
Buka Apps GS Real Device
  Open Application    ${REMOTE_URL}    platformName=${platformName}    platformVersion=${version}    deviceName=${deviceName}    app=${app}    udid=${udid}
  ...    automationName=${automationName}    noReset=False

Splash Screen GS
  ${Permission_notif}    Run Keyword And Return Status    Wait Until Element Is Visible     //XCUIElementTypeStaticText[@name="“GueSehat” Would Like to Send You Notifications"]
  Run Keywords    Permission_notif
  ...   AND   Run Keyword    Permission_Location
  ${Permission_Location}    Run Keyword And Return Status    Wait Until Element Is Visible     //XCUIElementTypeStaticText[@name="Allow “GueSehat” to access your location while you are using the app?"]
  Run Keywords    Permission_Location
  ...   AND   Run Keyword    Permission_notif
  # Permission_notif
  # Permission_Location
  # next Splash Screen
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="on_boarding_1"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Next"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Next"]
  # skip Splash Screen
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="on_boarding_2"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Skip"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Skip"]

klik cari
  Click A Point    x=665    y=1216

Refresh Halaman
  Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
