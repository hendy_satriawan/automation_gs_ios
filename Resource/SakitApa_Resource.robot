*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Sakit Apa
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Sakit Apa"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Sakit Apa"]
  # popup info
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Perhatian"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK"]
  # cek halaman sakit apa
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Sakit Apa"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="MULAI"]    ${timeout}
