*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DateTime

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
${emaillogin}   hendy.satriawan@gmail.com
${passlogin}    1234567


*** Keywords ***
Buka semua Tab Halaman Register
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="logo"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="DAFTAR"]    ${timeout}
  # masuk ke tab masuk / login
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Masuk, tab, 2 of 3"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Masuk, tab, 2 of 3"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  # masuke ke tab lupa sandi
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Lupa Sandi, tab, 3 of 3"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Lupa Sandi, tab, 3 of 3"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="send_forgot_password"]    ${timeout}

Skip Ke Home
  #pilih skip untuk masuk ke home
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Skip"]   ${timeout}
  ${start}    Get Current Date
  Click Element    //XCUIElementTypeButton[@name="Skip"]
  # cek masuk ke halaman home
  Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  ${end}    Get Current Date
  ${diff}   Subtract Date From Date    ${end}    ${start}   verbose
  Set Tags    [ TIME ] Loading Muncul Home via Tombol Skip: ${diff}

Login Google
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="logo"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="DAFTAR"]    ${timeout}
  # masuk ke tab masuk / login
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Masuk, tab, 2 of 3"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Masuk, tab, 2 of 3"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="google"]    ${timeout}
  # pilih google
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="google"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="google"]
  Permission_Google
  #kalau device belum pernah login
  ${belumlogin}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="Email or phone"]    ${timeout}
  Run Keyword If    ${belumlogin}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Next"]   ${timeout}
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="Email or phone"]    ${timeout}
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Done"]
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Next"]   ${timeout}
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Next"]
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${email_login_google}"]    ${timeout}
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="Enter your password"]     ${timeout}
  ...   AND   Input Text    //XCUIElementTypeSecureTextField[@name="Enter your password"]    ${pass_login_google}
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Next"][@visible="true"]     ${timeout}
  # ...   AND   ${start}    Get Current Date
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Next"][@visible="true"]
  ...   AND   Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  # ...   AND   ${end}    Get Current Date
  # ...   AND   ${diff}   Subtract Date From Date    ${end}    ${start}   verbose
  # ...   AND   Set Tags    [ TIME ] Loading Muncul Home via Login Google : ${diff}
  #masuk halaman google (sudah login)
  ${sudahlogin}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeLink[@label="${email_login_google}"]   10s
  Run Keyword If    ${sudahlogin}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeLink[@label="${nama_akun_google}"]   10s
  # ...   AND   ${start}    Get Current Date
  ...   AND   Click Element    //XCUIElementTypeLink[@name="${email_login_google}"]
  ...   AND   Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  # ...   AND   ${end}    Get Current Date
  # ...   AND   ${diff}   Subtract Date From Date    ${end}    ${start}   verbose
  # ...   AND   Set Tags    [ TIME ] Loading Muncul Home via Login Google : ${diff}
  # cek sudah berhasil login
  Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_akun_google}"]    ${timeout}

Login Facebook
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="logo"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="DAFTAR"]    ${timeout}
  # masuk ke tab masuk / login
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Masuk, tab, 2 of 3"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Masuk, tab, 2 of 3"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="facebook"]    ${timeout}
  # pilih facebook
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="facebook"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="facebook"]
  Permission_Facebook
  #kalau device belum pernah login
  ${belumloginfb}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Nomor ponsel atau email"]    ${timeout}
  Run Keyword If    ${belumloginfb}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Masuk"]   ${timeout}
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeTextField[@value="Nomor ponsel atau email"]    ${timeout}
  ...   AND   Input Text    //XCUIElementTypeTextField[@value="Nomor ponsel atau email"]    ${email_fb}
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Done"]
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@value="Kata Sandi Facebook"]
  ...   AND   Input Text    //XCUIElementTypeSecureTextField[@value="Kata Sandi Facebook"]    ${pass_fb}
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Done"]
  ...   AND   Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Masuk"]
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Masuk"]

  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Would you like to continue?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Continue"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Continue"]
  Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}

Login Via Side Menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  #pilih login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Masuk"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Masuk"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Skip"]    ${timeout}
  #masuk ke tab masuk
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Masuk, tab, 2 of 3"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Masuk, tab, 2 of 3"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}
  Tap    //XCUIElementTypeTextField[@name="email"]
  Input Text    //XCUIElementTypeTextField[@name="email"]    ${emaillogin}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # input password
  Tap    //XCUIElementTypeSecureTextField[@name="password"]     ${timeout}
  Input Password    //XCUIElementTypeSecureTextField[@name="password"]    ${passlogin}
  Click Element    //XCUIElementTypeButton[@name="Return"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="LOGIN"]
  # login sukses, masuk ke halaman home
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]    ${timeout}
