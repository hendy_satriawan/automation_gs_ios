*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Kembali ke Home dari Profile
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  # verifikasi halaman home
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]    ${timeout}

Masuk & Edit Profile
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  # pilih profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_akun_login_gs}"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Tulis Artikel"]   ${timeout}
  # masuk ke profile
  Click Element    //XCUIElementTypeStaticText[@name="${nama_akun_login_gs}"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Profil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_akun_login_gs}"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Edit Profile"]   ${timeout}
  # pilih tab artikel
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Artikel, tab, 2 of 3"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Artikel, tab, 2 of 3"]
  # buka tab publish
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Publish"][@value="1"]   ${timeout}
  # buka tab moderation
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Moderation"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Moderation"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Moderation"][@value="1"]     ${timeout}
  # buka tab draft
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Draft"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Draft"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Draft"][@value="1"]     ${timeout}
  # buka tab liked
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Liked"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Liked"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Liked"][@value="1"]     ${timeout}
  # masuk ke edit profile
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Edit Profile"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Edit Profile"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Simpan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Nama Depan"]   ${timeout}
  # input foto
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ganti Foto"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ganti Foto"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ambil Gambar"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Photo Gallery"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Photo Gallery"]
  # ambil foto
  ${capability}   Get Capability    platformVersion
  Log    ${capability}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Run Keyword If    '${capability}' == '10.2'    Click Element    //XCUIElementTypeButton[@name="Cancel"]
  Run Keyword If    '${capability}' == '12.1'   Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Camera Roll"]
  ...    AND    Click Element    //XCUIElementTypeCell[@name="Camera Roll"]
  ...    AND    Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]     ${timeout}
  ...    AND    Click Element    //XCUIElementTypeCell[@name="Photo, Landscape, August 09, 2012, 1:52 AM"]
  ...    AND    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Choose"]   ${timeout}
  ...    AND    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  ...    AND    Click Element    //XCUIElementTypeButton[@name="Choose"]
  ...    AND    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Success"]    ${timeout}
  ...    AND    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK"]   ${timeout}
  ...    AND    Click Element    //XCUIElementTypeButton[@name="OK"]
  Sleep    2s
  # klik simpan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Simpan"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Simpan"]
  # berhasil tersimpan - kembali ke halaman profile
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil anda telah diperbaharui"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK"]
  # cek halaman detil profil
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Profil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_akun_login_gs}"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Edit Profile"]   ${timeout}
