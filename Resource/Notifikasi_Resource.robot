*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Notifikasi Side Menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  # buka drawer menu notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Notification"]     ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Notification"]
  # masuk ke halaman notifikasi
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Notification"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]     ${timeout}
  # masuk ke halaman cari
  Click Element    //XCUIElementTypeButton[@name="icon search"]
  # cek halaman cari
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
