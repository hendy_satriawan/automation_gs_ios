*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Kembali ke Event dari Detil Event
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Event"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share android"]   ${timeout}


Masuk Halaman Event
  #buka side menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  # pilih menu event
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Event"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Event"]
  # masuk halaman event
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Event"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share android"]   ${timeout}
  # masuk ke detail event
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="FREE"]     ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="FREE"]
  # masuk ke detail event
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="About Event"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Deskripsi"]   ${timeout}
  # scroll sampai halaman bawah
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-event}   Convert To String    ${lebars}
  ${x2-event}   Convert To String    ${lebars}
  ${y1-event}   Convert To String    ${tinggis}
  ${y2-event}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel berikutnya)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Lokasi"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-event}    ${y1-event}    ${x2-event}    ${y2-event}
  \    ${loopCount}    Set Variable    ${loopCount}+1

Share Event
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share android"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share android"]
  # muncul opsi share
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  # kembali ke list event
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # cek halaman list
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Event"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share android"]   ${timeout}
