*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Variables ***
${judulcariartikel}   Mengapa Semua Orang Menyukai Diskon?
${judulartikelpagination}    Saat Ini, Stres Meningkat di Kalangan Usia 20-an!
${judulartikelslideshow}    10 Alasan Ilmiah Mengapa Pria Harus Bermain Gitar
${judulartikelcounting}     Apakah Kamu Dehidrasi?
${judulartikeltrivia}     Kuis: Apakah Kamu Memiliki Gangguan Kecemasan?
${judulartikelsurvey}     Survei: Apakah Kamu Meredakan Stres dengan Melakukan Kegiatan Seni?

*** Keywords ***
Kembali ke list Artikel menu dari Detil Artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman artikel
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Artikel"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Semua')]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon tulisartikel"]    ${timeout}

Kembali ke Artikel dari Pencarian Artikel
  ${cekhome}    Run Keyword And Return Status    //XCUIElementTypeButton[@name="Cancel"]
  Run Keyword Unless    ${cekhome}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  ...    AND    Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # halaman hasil pencarian
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # verifikasi halaman artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Artikel"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon tulisartikel"]   ${timeout}

Kembali ke Home dari Artikel Search
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # cek bila tidak langsung kembali ke HOME
  ${ceksearch}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]
  Run Keyword If    ${ceksearch}    Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # verifikasi halaman home
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="logo"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Home"][@value="1"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Dokter"]     ${timeout}

Kembali ke Home dari Artikel Slideshow
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  # halaman hasil pencarian
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'ARTIKEL')][@selected='true']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')][@text='Batal']    ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')][@text='Batal']
  # verifikasi halaman home
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]    ${timeout}

Kembali ke Home dari Artikel Counting
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  # halaman hasil pencarian
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'ARTIKEL')][@selected='true']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')][@text='Batal']    ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')][@text='Batal']
  # verifikasi halaman home
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]    ${timeout}

Kembali ke Home dari Artikel Trivia
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  # halaman hasil pencarian
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'ARTIKEL')][@selected='true']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')][@text='Batal']    ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')][@text='Batal']
  # verifikasi halaman home
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]    ${timeout}

Kembali ke Home dari Artikel Survey
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  # halaman hasil pencarian
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'ARTIKEL')][@selected='true']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')][@text='Batal']    ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')][@text='Batal']
  # verifikasi halaman home
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]    ${timeout}

Masuk Artikel via Side Menu & Detail Semua artikel
  # buka side menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Masuk"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel"][@visible="true"]    ${timeout}
  # masuk ke menu artikel
  Click Element    //XCUIElementTypeStaticText[@name="Artikel"][@visible="true"]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Artikel"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Semua')]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon tulisartikel"]    ${timeout}

Masuk Artikel via Side Menu & Detail Lifestyle Artikel
  # pindah ke tab Lifestyle
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle')]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Artikel"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon tulisartikel"]    ${timeout}
  # masuk ke detail artikel - Lifestyle
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle •')]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle •')]
  # masuk ke artikel detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle •')]
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Masuk Artikel via Side Menu & Detail Medis Artikel
  # pindah ke tab Medis
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Medis')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Medis')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Medis')]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Artikel"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon tulisartikel"]    ${timeout}
  # masuk ke detail artikel - Medis
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Medis •')]
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Medis •')]
  # masuk ke artikel detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Medis •')]
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Masuk Artikel via Side Menu & Detail Sex Love Artikel
  # pindah ke tab Sex & Love
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Sex & Love')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Sex & Love')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Sex & Love')]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Artikel"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon tulisartikel"]    ${timeout}
  # masuk ke detail artikel - Sex & Love
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Sex & Love •')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Sex & Love •')]
  # masuk ke artikel detail
  ${warning}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel ini mengandung konten dewasa, diharapkan kebijakan Anda dalam membaca"]   10s
  Run Keyword If    ${warning}    Click Element    //XCUIElementTypeButton[@name="setuju"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Sex & Love •')]   ${timeout}
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Masuk Artikel via Side Menu & Detail Wanita Artikel
  # pindah ke tab Wanita
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Wanita')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Wanita')]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Wanita')]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Artikel"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon tulisartikel"]    ${timeout}
  # masuk ke detail artikel - wanita
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Wanita •')]
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Wanita •')]
  # masuk ke artikel detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Wanita •')]
  # bagikan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon share ios"]
  # kembali ke detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]

Cari Artikel & Masuk ke Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon search"]
  # masuk halaman pencarian
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  Input Text    //XCUIElementTypeSearchField[@name="Cari"]    ${judulcariartikel}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # tampil yang dicari
  ${cariartikelnya}   Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'${judulcariartikel}')]   ${timeout}
  Run Keyword If    ${cariartikelnya}    Click Element    //XCUIElementTypeStaticText[contains(@name,'${judulcariartikel}')]
  # masuk ke detail artikel
  Run Keyword If    ${cariartikelnya}    Run Keywords    Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'${judulcariartikel}')]   ${timeout}
  ...   AND     Wait Until Page Contains Element    //XCUIElementTypeButton[@name="message outline"]    ${timeout}
  # kalau yag dicari gak ada
  Run Keyword Unless    ${cariartikelnya}    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/tv_cancel')][@text='Batal']   ${timeout}

Artikel Pagination & Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon search"]
  # masuk halaman pencarian
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  Input Text    //XCUIElementTypeSearchField[@name="Cari"]    ${judulartikelpagination}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # tampil yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikelpagination}')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikelpagination}')]
  # masuk ke detail artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  # scroll sampai dapat pagination
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai bawah (sampai dapat artikel berikutnya)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="1 of 2"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon Next Round"]   ${timeout}
  # ke next page
  Click Element    //XCUIElementTypeButton[@name="icon Next Round"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Bagaimana Cara Mengatasi Stres?"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="1. Berpikir positif"]    ${timeout}
  #Scroll artikel sampai bawah (sampai dapat artikel berikutnya)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="2 of 2"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon Back Round"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon Back Round"]
  # cek sudah kembali ke halaman pagination 1
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]    ${timeout}
  Page Should Not Contain Element    //XCUIElementTypeButton[@name="icon Back Round"]
  # ke halaman atas
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - ${tinggiy}
  ${x1-hal1}   Convert To String    ${lebars}
  ${x2-hal1}   Convert To String    ${lebars}
  ${y1-hal1}   Convert To String    ${tinggis}
  ${y1-hal1}   Evaluate    ${tinggis} + 300
  ${y2-hal1}   Evaluate    ${y1-hal1} + 500
  Log    ${x1-hal1}
  Log    ${x2-hal1}
  Log    ${y1-hal1}
  Log    ${y2-hal1}
  #Scroll artikel sampai atas (sampai dapat tombol ubah status sudah hamil)
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${eh}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="${judulartikelpagination}"]
  \    Run Keyword If    ${eh}    Exit For Loop
  \    Swipe    ${x1-hal1}    ${y1-hal1}    ${x2-hal1}    ${y2-hal1}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Page Should Not Contain Element    //XCUIElementTypeStaticText[@name="Bagaimana Cara Mengatasi Stres?"]

Artikel Slideshow & Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon search"]
  # masuk halaman pencarian
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  Input Text    //XCUIElementTypeSearchField[@name="Cari"]    ${judulartikelslideshow}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # tampil yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikelslideshow}')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikelslideshow}')]
  # masuk ke detail artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  # scroll sampai dapat slideshow
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai dapat slideshow
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon Next"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    3s
  # klik next slide
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${next}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon Next"]
  \    Run Keyword Unless    ${next}    Exit For Loop
  \    Sleep    1s
  \    Click Element    //XCUIElementTypeButton[@name="icon Next"]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="10 of 10"]   ${timeout}
  Wait Until Page Does Not Contain Element    //XCUIElementTypeButton[@name="icon Next"]
  # klik prev slide
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${next}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon Back"]
  \    Run Keyword Unless    ${next}    Exit For Loop
  \    Click Element    //XCUIElementTypeButton[@name="icon Back"]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="1 of 10"]   ${timeout}
  Wait Until Page Does Not Contain Element    //XCUIElementTypeButton[@name="icon Back"]

Artikel Counting & Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon search"]
  # masuk halaman pencarian
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  Input Text    //XCUIElementTypeSearchField[@name="Cari"]    ${judulartikelcounting}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # tampil yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikelcounting}')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikelcounting}')]
  # masuk ke detail artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  # scroll sampai dapat slideshow
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai dapat counting
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="SUBMIT"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    3s
  # klik CheckBox
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jarang buang air kecil"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Jarang buang air kecil"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kulit kering"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Kulit kering"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Nyeri otot"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Nyeri otot"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SUBMIT"]   ${timeout}
  # submit
  Click Element    //XCUIElementTypeButton[@name="SUBMIT"]
  # masuk ke halaman result
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Your Result:"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BAGIKAN HASIL"]    ${timeout}
  #Scroll artikel sampai dapat tombol ulangi kuis
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="ULANGI KUIS"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1

Artikel Trivia & Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon search"]
  # masuk halaman pencarian
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  Input Text    //XCUIElementTypeSearchField[@name="Cari"]    ${judulartikeltrivia}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # tampil yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikeltrivia}')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikeltrivia}')]
  # masuk ke detail artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  # scroll sampai dapat kuis trivia
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai dapat slideshow
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Tidak"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    3s
  # klik pertanyaan 1
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Iya"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Iya"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 2
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Iya"][@visible="true"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Iya"][@visible="true"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 3
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kehilangan selera makan"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Kehilangan selera makan"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 4
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Pria"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Pria"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 5
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Mengalami perceraian"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Mengalami perceraian"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 6
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tidak bisa"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tidak bisa"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 7
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Bisa"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Bisa"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 8
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Sulit Tidur"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Sulit Tidur"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 9
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Olahraga"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Olahraga"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 10
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Keduanya benar"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Keduanya benar"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="BERIKUTNYA"]
  # klik pertanyaan 11
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Salah"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Salah"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LIHAT HASIL"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="LIHAT HASIL"]
  # validasi halaman result
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Skor Kamu:"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="SHARE"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="BAGIKAN HASIL"][@visible="true"]   ${timeout}

Artikel Survey & Detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon search"]
  # masuk halaman pencarian
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]    ${timeout}
  Input Text    //XCUIElementTypeSearchField[@name="Cari"]    ${judulartikelsurvey}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # tampil yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikelsurvey}')]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'${judulartikelsurvey}')]
  # masuk ke detail artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}
  # scroll sampai dapat CheckBox
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-artikel}   Convert To String    ${lebars}
  ${x2-artikel}   Convert To String    ${lebars}
  ${y1-artikel}   Convert To String    ${tinggis}
  ${y2-artikel}   Evaluate    ${tinggis} - 700
  #Scroll artikel sampai dapat CheckBox
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="SELANJUTNYA"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    3s
  # klik CheckBox 1
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tidak"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tidak"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SELANJUTNYA"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="SELANJUTNYA"]
  # klik CheckBox 2
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Menuliskan hal yang membuat stres"][@visible="true"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Menuliskan hal yang membuat stres"][@visible="true"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SELANJUTNYA"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="SELANJUTNYA"]
  # klik CheckBox 3
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Jika ada buku mewarnai, apakah Kamu akan menggunakannya?"][@visible="true"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Tidak"][@visible="true"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Tidak"][@visible="true"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SELANJUTNYA"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="SELANJUTNYA"]
  # klik CheckBox 4
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Kegiatan apa yang Kamu lakukan untuk menghilangkan stres?"][@visible="true"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Sangat stres, sampai tidak bisa tidur"][@visible="true"]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Sangat stres, sampai tidak bisa tidur"][@visible="true"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="SELANJUTNYA"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="SELANJUTNYA"]
  # masuk ke halaman result
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Great Job!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Terima Kasih atas Partisipasinya dalam Survey ini"]   ${timeout}
