*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Ke Home
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Home"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Home"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="logo"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Home"][@value="1"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Dokter"]     ${timeout}

Kembali ke Home dari slide artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman home
  Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}


Kembali ke Home via side menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Beranda"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Beranda"]
  # verifikasi halaman home
  # Wait Until Page Does Not Contain Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/label')][@text='Please wait']    ${timeout}
  Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}

Kembali ke Home via Bottom Bar
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Home"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Home"]
  # verifikasi halaman home
  # Wait Until Page Does Not Contain Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/label')][@text='Please wait']    ${timeout}
  Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}

Kembali ke Home via Arrow Back dari Artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # cek halaman home
  Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}


Kembali ke search dari detail
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Back Arrow"]
  # verifikasi halaman search
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}

Kembali ke Home dari Search
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  Click Element    //XCUIElementTypeButton[@name="Cancel"]
  # veridikasi halaman home
  Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}

Home Slide Artikel
  Wait Until Element Is Visible    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  # click splash screen
  Click Element    //XCUIElementTypeApplication[@name="GueSehat"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]
  # check slide artikel & view
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   60s
  Wait Until Page Contains Element    //XCUIElementTypeLink[@name="VIEW"]   60s

Dokter Slide Direktori
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Dokter"]    ${timeout}
  #ambil kordinat element
  ${dirslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Dokter"]
  ${dirslide}    Convert To String    ${dirslide}
  ${remove}   Remove String    ${dirslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat direktori yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Dokter"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Dokter"]
  #masuk ke direktori dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Dokter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="dir_rs_icon"]   ${timeout}


Rumah sakit slide Direktori
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Rumah Sakit"]    ${timeout}
  #ambil kordinat element
  ${dirslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Rumah Sakit"]
  ${dirslide}    Convert To String    ${dirslide}
  ${remove}   Remove String    ${dirslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Rumah Sakit"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Rumah Sakit"]
  #masuk ke direktori rumah sakit
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Rumah Sakit"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]   ${timeout}

Klinik slide Direktori
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Klinik"]    ${timeout}
  #ambil kordinat element
  ${dirslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Klinik"]
  ${dirslide}    Convert To String    ${dirslide}
  ${remove}   Remove String    ${dirslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Klinik"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Klinik"]
  #masuk ke direktori Klinik
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Klinik"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Klinik"]   ${timeout}

Gym & Health Club slide Direktori
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Gym & Health Club"]    ${timeout}
  #ambil kordinat element
  ${dirslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Gym & Health Club"]
  ${dirslide}    Convert To String    ${dirslide}
  ${remove}   Remove String    ${dirslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Gym & Health Club"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Gym & Health Club"]
  #masuk ke direktori gym
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Gym & Health Club"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]   ${timeout}

Spa & Massage slide Direktori
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]    ${timeout}
  #ambil kordinat element
  ${dirslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Spa & Massage"]
  ${dirslide}    Convert To String    ${dirslide}
  ${remove}   Remove String    ${dirslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Spa & Massage"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]
  #masuk ke direktori spa
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Spa & Massage"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]   ${timeout}

Healthy Food & Beverage slide Direktori
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]    ${timeout}
  #ambil kordinat element
  ${dirslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Spa & Massage"]
  ${dirslide}    Convert To String    ${dirslide}
  ${remove}   Remove String    ${dirslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Healthy Food & Beverage"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Healthy Food & Beverage"]
  #masuk ke direktori spa
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Healthy Food & Beverage"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]   ${timeout}

Beauty slide Direktori
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]    ${timeout}
  #ambil kordinat element
  ${dirslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Spa & Massage"]
  ${dirslide}    Convert To String    ${dirslide}
  ${remove}   Remove String    ${dirslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Beauty"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Beauty"]
  #masuk ke direktori spa
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Beauty"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]   ${timeout}

Lab slide Direktori
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]    ${timeout}
  #ambil kordinat element
  ${dirslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Spa & Massage"]
  ${dirslide}    Convert To String    ${dirslide}
  ${remove}   Remove String    ${dirslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Lab"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Lab"]
  #masuk ke direktori spa
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Lab"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]   ${timeout}

Praktisi slide Direktori
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Spa & Massage"]    ${timeout}
  #ambil kordinat element
  ${dirslide}    Get Element Location    //XCUIElementTypeStaticText[@name="Spa & Massage"]
  ${dirslide}    Convert To String    ${dirslide}
  ${remove}   Remove String    ${dirslide}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //XCUIElementTypeStaticText[@name="Praktisi"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //XCUIElementTypeStaticText[@name="Praktisi"]
  #masuk ke direktori spa
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Praktisi"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]   ${timeout}

Home Artikel kategori Lifestyle
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Lifestyle, tab, 1 of 4"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Home"][@value="1"]   ${timeout}
  #buka artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle')]
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle')]
  #masuk ke artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]  ${timeout}


Home Artikel kategori Medis
  Wait Until Element Is Visible    //XCUIElementTypeCell[@name="Medis, tab, 2 of 4"]
  Click Element    //XCUIElementTypeCell[@name="Medis, tab, 2 of 4"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Medis, tab, 2 of 4"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Medis')]   ${timeout}
  # buka artikel
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Medis')]
  # masuk artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]  ${timeout}

Home Artikel kategori Sex & Love
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Sex & Love, tab, 3 of 4"]
  Click Element    //XCUIElementTypeCell[@name="Sex & Love, tab, 3 of 4"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Sex & Love, tab, 3 of 4"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Sex & Love •')]   ${timeout}
  # buka artikel
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Sex & Love •')]
  # masuk artikel terkena 17+
  ${warning}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Artikel ini mengandung konten dewasa, diharapkan kebijakan Anda dalam membaca"]   10s
  Run Keyword If    ${warning}    Click Element    //XCUIElementTypeButton[@name="setuju"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Sex & Love •')]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]  ${timeout}

Home Artikel kategori Wanita
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Wanita, tab, 4 of 4"]
  Click Element    //XCUIElementTypeCell[@name="Wanita, tab, 4 of 4"]
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Wanita, tab, 4 of 4"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Wanita')]   ${timeout}
  # buka artikel
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Wanita')]
  # masuk artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]  ${timeout}

Search Home Artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon search"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon search"]
  # input yang dicari
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  Tap    //XCUIElementTypeSearchField[@name="Cari"]
  Input Text    //XCUIElementTypeSearchField[@name="Cari"]    ${yangdicari}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # muncul yang dicari
  # buka semua tab -- Artikel - Forum - Dokter
  # tab artikel
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle')]    ${timeout}
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Lifestyle')]
  #masuk ke halaman artikel
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon like off"]  ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]   ${timeout}

Search Home Forum
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  # buka semua tab -- Artikel - Forum - Dokter
  # tab artikel
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Forum')]    ${timeout}
  # pilih tab forum
  Click Element    //XCUIElementTypeCell[contains(@name,'Forum')]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="icon_comment"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="icon_view"]
  Click Element    //XCUIElementTypeStaticText[contains(@name,'Anonim')]
  #masuk ke halaman forum
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Forum"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]    ${timeout}

Search Home Dokter
  Wait Until Page Contains Element    //XCUIElementTypeSearchField[@name="Cari"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Cancel"]   ${timeout}
  # buka semua tab -- Artikel - Forum - Dokter
  # tab dokter
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Dokter')]    ${timeout}
  # pilih tab dokter
  Click Element    //XCUIElementTypeCell[contains(@name,'Dokter')]
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Rating"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="dir_rs_icon"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="dir_waktu_icon"]    ${timeout}
  Click Element    //XCUIElementTypeOther[@name="Rating"]
  #masuk ke halaman dokter
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Back Arrow"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Dokter"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon share ios"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeCell[contains(@name,'Profil')]   ${timeout}
