*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Variables ***
${back_keycode}   4

*** Keywords ***
Buka Bukes
  #masuk ke halaman bukes via bottom menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Bukes"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Bukes"]
  # cek halaman bukes
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Buku Kesehatan"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Hitung_kalori_IMT"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="siklus_haid"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kehamilan"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="tumbuh_kembang_imunisasi"]

Masuk Hitung Kalori IMT
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Hitung_kalori_IMT"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Hitung_kalori_IMT"]
  # cek halaman IMT
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Index Massa Tubuh"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="MULAI"]    ${timeout}

Masuk Siklus Haid
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="siklus_haid"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="siklus_haid"]
  # masuk ke halaman siklus haid
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Siklus Haid"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="MULAI"]    ${timeout}

Masuk Kehamilan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kehamilan"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Kehamilan"]
  # masuk ke halaman kehamilan
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Kehamilan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="MULAI"]    ${timeout}

Masuk Tumbuh Kembang
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="tumbuh_kembang_imunisasi"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="tumbuh_kembang_imunisasi"]
  # masuk ke halaman tumbuh kembang
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Tumbuh Kembang Anak"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Daftarkan Anak"]    ${timeout}
