*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot

*** Keywords ***
Register
  Skip Ke Home
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  #open side menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar Baru"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ayo dapatkan Poin Anda"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar Baru"]
  #masuk ke halaman Register
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Daftar Baru, tab, 1 of 3"]    ${timeout}
  #input data untuk register
  #input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="nama_depan"]     ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="nama_depan"]    ${first_name}
  Click Element    //XCUIElementTypeButton[@name="Toolbar Done Button"]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="nama_belakang"]    ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="nama_belakang"]    ${last_name}
  Click Element    //XCUIElementTypeButton[@name="Toolbar Done Button"]
  # input email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="alamat_email"]    ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="alamat_email"]    ${email_regis}
  Click Element    //XCUIElementTypeButton[@name="Toolbar Done Button"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="password"]    ${timeout}
  Input Text    //XCUIElementTypeSecureTextField[@name="password"]    ${pass_regis}
  Click Element    //XCUIElementTypeButton[@name="Toolbar Done Button"]
  #checklist newsletter & syarat ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="tnc_setuju"]   ${timeout}
  Click Element    //XCUIElementTypeImage[@name="tnc_setuju"]
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="daftar_newsletter"]    ${timeout}
  Click Element    //XCUIElementTypeImage[@name="daftar_newsletter"]
  #klik tombol daftar
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="DAFTAR"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="DAFTAR"]
  # cek register berhasil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Registrasi Berhasil!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK"]
  Click Element    //XCUIElementTypeButton[@name="OK"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LOGIN"]    ${timeout}

Forgot Password
  Skip Ke Home
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="icon menu"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="icon menu"]
  #open side menu
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar Baru"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ayo dapatkan Poin Anda"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar Baru"]
  #masuk ke halaman Register
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Daftar Baru, tab, 1 of 3"]    ${timeout}
  #pilih tab lupa password
  Wait Until Page Contains Element    //XCUIElementTypeCell[@name="Lupa Sandi, tab, 3 of 3"]   ${timeout}
  Click Element    //XCUIElementTypeCell[@name="Lupa Sandi, tab, 3 of 3"]
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="alamat_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="send_forgot_password"]   ${timeout}
  # input email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="alamat_email"]   ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="alamat_email"]    ${email_regis}
  Click Element    //XCUIElementTypeButton[@name="Toolbar Done Button"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="send_forgot_password"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="send_forgot_password"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Link berhasil dikirimkan ke email anda"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK"]

Verifikasi akun setelah forgot password
  # masuk ke halaman verifikasi
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/title_textview')][@text='Verifikasi']    ${timeout}
  Wait Until Page Contains Element    //TextInputLayout[contains(@resource-id,'com.guesehat.android:id/til_input')][@text='Email']    ${timeout}
  Input Text    //TextInputLayout[contains(@resource-id,'com.guesehat.android:id/til_input')][@text='Email']    ${email_regis}
  Hide Keyboard
  Click Element    //android.widget.Button[contains(@resource-id,'com.guesehat.android:id/btn_verif')][@text='VERIFIKASI']
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.guesehat.android:id/snackbar_text')][@text='Mohon cek Email Anda untuk melakukan proses verifikasi lebih lanjut']    ${timeout}
  #ambil kode dulu di email
  Press Keycode    3    #keluar ke home
  #buka chrome
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@content-desc,'Chrome')]    ${timeout}
  Click Element    //android.widget.FrameLayout[contains(@content-desc,'Chrome')]
  #newtab
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@resource-id,'com.android.chrome:id/tab_switcher_button')]    ${timeout}
  Click Element    //android.widget.ImageButton[contains(@resource-id,'com.android.chrome:id/tab_switcher_button')]
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.android.chrome:id/new_tab_button')]    ${timeout}
  Click Element    //android.widget.Button[contains(@resource-id,'com.android.chrome:id/new_tab_button')]
  #cek halaman awal google
  #masuk yopmail
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]   5s
  Click Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]
  Input Text    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/url_bar')]    yopmail.com
  Press Keycode    66   #enter
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'login')]   ${timeout}
  Clear Text    //android.widget.EditText[contains(@resource-id,'login')]
  Input Text    //android.widget.EditText[contains(@resource-id,'login')]    ${email_regis}
  Hide Keyboard
  Sleep    1s
  Press Keycode    66   #enter
  Wait Until Page Contains Element    //android.view.View[contains(@text,'today')]    ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'m1')]    ${timeout}
  #buka email
  Click Element    //android.view.View[contains(@resource-id,'m1')]
  Wait Until Page Contains Element    //android.view.View[contains(@text,'Verifikasi Akun')]   ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@text,'From: Guesehat <info@guesehat.com>')]   ${timeout}
  Sleep    2s
  # verifikasi email
  Click Element    //android.view.View[contains(@text,'Verifikasi Sekarang')]
  #masuk ke aplikasi gue sehat dengan deeplink
  ${deeplinkgs}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'android:id/text1')][@text='GueSehat']   ${timeout}
  Run Keyword If    ${deeplinkgs}    Click Element    //android.widget.TextView[contains(@resource-id,'android:id/text1')][@text='GueSehat']
  Wait Until Page Contains Element    //android.view.View[contains(@text,'Terimakasih sudah bergabung di guesehat.com. Akun anda sudah terverifikasi')]   ${timeout}
  #recent apps (kembali ke halaman chrome)
  Press Keycode    187  #recent apps
  Sleep    1s
  Press Keycode    187  #recent apps
  #back & hapus semua email
  Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'rethome')]   ${timeout}
  Click Element    //android.view.View[contains(@resource-id,'rethome')]
  Sleep    2s
  Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'e0')]    ${timeout}
  Click Element    //android.view.View[contains(@resource-id,'e0')]
  Sleep    2s
  Click Element    //android.view.View[contains(@text,'javascript:suppr_sel();')]
  # kembali ke aplikasi GS
  Press Keycode    187  #recent apps
  Sleep    1s
  Press Keycode    187  #recent apps
  # back ke halaman home
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Click Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]
  Wait Until Page Contains Element    //android.widget.RelativeLayout[contains(@resource-id,'com.guesehat.android:id/btn_home')]    ${timeout}

Reset Password diemail
  #ambil kode dulu di email
  Press Keycode    3    #keluar ke home
  #buka chrome
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@content-desc,'Chrome')]    ${timeout}
  Click Element    //android.widget.FrameLayout[contains(@content-desc,'Chrome')]
  #newtab
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@resource-id,'com.android.chrome:id/tab_switcher_button')]    ${timeout}
  Click Element    //android.widget.ImageButton[contains(@resource-id,'com.android.chrome:id/tab_switcher_button')]
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.android.chrome:id/new_tab_button')]    ${timeout}
  Click Element    //android.widget.Button[contains(@resource-id,'com.android.chrome:id/new_tab_button')]
  #cek halaman awal google
  #masuk yopmail
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]   5s
  Click Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]
  Input Text    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/url_bar')]    yopmail.com
  Press Keycode    66   #enter
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'login')]   ${timeout}
  Clear Text    //android.widget.EditText[contains(@resource-id,'login')]
  Input Text    //android.widget.EditText[contains(@resource-id,'login')]    ${email_regis}
  Hide Keyboard
  Sleep    1s
  Press Keycode    66   #enter
  Wait Until Page Contains Element    //android.view.View[contains(@text,'today')]    ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'m1')]    ${timeout}
  #buka email
  Click Element    //android.view.View[contains(@resource-id,'m1')]
  # klik link ganti password
  Wait Until Page Contains Element    //android.view.View[contains(@text,'Lupa Password')]    ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@text,'From: Guesehat <info@guesehat.com>')]   ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@text,'Ganti Password')]   ${timeout}
  Sleep    2s
  Click Element    //android.view.View[contains(@text,'Ganti Password')][@index='4'][@instance='40']
  Sleep    15s
  #masuk ke halaman input password (aplikasi guesehat)
  Wait Until Page Contains Element    //android.widget.ImageButton[contains(@content-desc,'Navigasi naik')]   ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@text,'Silakan ganti password anda.')]   ${timeout}
  #close tanya dokter
  Wait Until Page Contains Element    //android.widget.EditText[contains(@index,'0')][@NAF='true']    ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@text,'KATA SANDI BARU')]    ${timeout}
  ${tanyadok}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.View[contains(@text,'X')][@index='0']
  Run Keyword If    ${tanyadok}   Click Element    //android.view.View[contains(@text,'X')][@index='0']
  #input password baru
  Wait Until Page Contains Element    //android.widget.EditText[contains(@index,'0')][@NAF='true']    ${timeout}
  Tap    //android.widget.EditText[contains(@index,'0')][@NAF='true']
  Input Text    //android.widget.EditText[contains(@index,'0')][@NAF='true']    ${pass_regis}
  Press Keycode    66   #enter
  Hide Keyboard
  #input konfirm new password
  Tap    //android.widget.EditText[contains(@index,'0')][@NAF='true']
  Input Text    //android.widget.EditText[contains(@index,'0')][@NAF='true']    ${pass_regis}
  Hide Keyboard
